"""
This is a sample hello world flask app
It only has a root resource which sends back hello World html text
"""

import logging
from xpresso.ai.core.data.inference import AbstractInferenceService
from xpresso.ai.core.logging.xpr_log import XprLogger

import numpy as np
import pandas as pd
import json
import pickle
from sklearn import metrics
import datetime as dt
from datetime import timedelta
from sklearn.metrics import mean_squared_log_error
from statsmodels.tsa.api import Holt

import os

__author__ = "### Author ###"

logger = XprLogger("inference_service", level=logging.INFO)


class InferenceService(AbstractInferenceService):
  """ Main class for the inference service. User will need to implement
    following functions:
       - load_model: It gets the directory of the model stored as parameters,
           user will need to implement the method for loading the model and
           updating self.model variable
       - transform_input: It gets the JSON object from the rest API as input.
           User will need to implement the method to convert the json data to
           relevant feature vector to be used for prediction
       - predict: It gets the feature vector or any other object from the
           transform_input method. User will need to implement the model
           prediction codebase here. It returns the predicted value
       - transform_output: It gets the predicted value from the predict method.
           User need to implement this method to converted predicted method
           to JSON serializable object. Response from this method is send back
           to the client as JSON Object.

    AbstractInferenceService automatically creates the flask reset api
    with resource /predict for this class.
    Request JSON format:
       {
         "input" : <input_goes_here> // It could be any JSON object
       }
       This value of "input" key is sent to transform_input

    Response JSON format:
       {
         "message": "success/failure",
         "results": <response goes here> // It could be any JSOn object
       }
       Output of transform_output goes as value of "results"
    """
  def __init__(self, config_file):
    super().__init__()
    """ Initialize any static data required during boot up """

    self.holts_smoothing_level = config_file['holts_winter']["smoothing_level"]
    self.holts_smoothing_slope = config_file['holts_winter']["smoothing_slope"]
    self.output_df = None
    self.prediction_count = config['prediction_count']
    self.output_file_path = None
    self.model_file_path = None
    self.pre_processed_file_path = None

  def spilt_train_valid(self, data, p):
    """[summary]

      Args:
          data ([type]): [description]
          p ([type]): [description]
      """
    data = data.sort_values(by=['Date'])
    train = data[:int(data.shape[0] * p)]
    valid = data[int(data.shape[0] * p):]
    return train, valid

  def inference_holt(self, model, country_name):
    """Method used for inference of holt

        Args:
            model (pkl): dict having models for all countries
            country_name ([type]): the country name for which prediction has to be made

        Returns:
            [type]: [description]
        """
    try:
      # splitting data into train and test:
      # train_data, valid_data = self.spilt_train_valid(self, covid_full_data, p)
      #
      # train_series = train_data.loc[train_data.Country ==
      #                               country_name].incremental_case_count
      # model = Holt(train_series.values).fit(smoothing_level=self.holts_smoothing_level,
      #                                       smoothing_slope=self.holts_smoothing_slope)

      # forecast values
      forecast_result = model.forecast(self.prediction_count)

      # if forecast is negative make it zero
      npos = np.where(forecast_result < 0)[0]
      for i in npos:
        forecast_result[i] = 0
      return forecast_result

    except Exception as err:
      logger.error(err)
      logger.error(country_name)

  def load_model(self, model_path):
    """ This is used to load the model on the boot time.
        User will need to load the model and save it in the variable
        self.model. It is IMPORTANT to update self.model.
        Args:
            model_path(str): Path of the directory where the model files are
               stored.
        """
    self.model_file_path = model_path + '/model.pickle'
    # self.model_file_path = 'D:/Hackathon/model.pickle'
    self.pre_processed_file_path = model_path + '/covid19_pre_processed.xlsx'
    # self.pre_processed_file_path = 'D:/Hackathon/covid19_pre_processed.xlsx'

  def transform_input(self, input_request):
    """
        Convert the raw input request to a feature data or any other object
        to be used during prediction

        Args:
            input_request: Currently the input is assumed to be in following format
            {  "test_data_file" : <local location of test_data file>,
               "predictions_location" : <local location where to save predicted file>
            }

        Returns:
        list: returning list of country names to forecast count.
    """
    print('inside transform_input')
    inp_data_test = pd.read_excel('/inf_data/' + input_request["test_data_file"])
    # inp_data_test = pd.read_excel(input_request["test_data_file"])
    self.output_file_path = '/inf_data/' + input_request['predictions_location']
    # create this output folder if not exists

    return inp_data_test.Country.unique().tolist()

  def predict(self, input_request):
    """
        This method implements the prediction method of the supported model.
        It gets the output of transform_input as input and returns the predicted
        value

        Args:
            input_request: Transformed object outputted from transform_input
               method

        Returns:
            obj: predicted value from the model

    """
    print('inside predict')
    pred_df = pd.DataFrame()
    # processed_data = preprocess_data(self.covid_full_data)
    model_file = {}
    with open(self.model_file_path, 'rb') as f:
      model_file = pickle.load(f)
    print(len(model_file))
    covid_full_data = pd.read_excel(self.pre_processed_file_path)
    covid_full_data["Date"] = pd.to_datetime(covid_full_data["Date"])
    for country_name in input_request:
      last_date_training_data = covid_full_data.loc[covid_full_data.Country ==
                                                    country_name].Date.max()
      print(last_date_training_data)
      forecast = self.inference_holt(model_file[country_name], country_name)
      forecast = [int(x) for x in forecast]

      list_of_14_dates = [
        last_date_training_data + pd.DateOffset(i)
        for i in range(1, self.prediction_count + 1)
      ]

      # Saving the predictions
      pred_c = pd.DataFrame()
      pred_c['Date'] = list_of_14_dates
      pred_c['Confirmed'] = forecast
      pred_c['Country'] = country_name

      pred_df = pred_df.append(pred_c, ignore_index=True)

    self.output_df = pred_df

    # country = "India"
    # prediction_count = 14
    # forecast = inference_holt(covid_full_data, holts_winter_params, country, prediction_count,1)
    # forecast = [int(x) for x in forecast]
    # print(forecast)

    return "Inference completed"

  def transform_output(self, output_response):
    """
        Convert the predicted value into a relevant JSON serializable object.
        This is sent back to the REST API call
        Args:
            output_response: Predicted output from predict method.

        Returns:
          obj: JSON Serializable object
        """
    print('inside transform_output')
    self.output_df = self.output_df.round({"Confirmed": 1})
    self.output_df[['Country', 'Date',
                    'Confirmed']].to_csv(self.output_file_path + 'predictions.csv',
                                         index=False)
    return "saved predictions file"


if __name__ == "__main__":
  config = {}
  with open("config/dev.json") as conf_file:
    config = json.load(conf_file)
  logging.info("Config loaded")
  pred = InferenceService(config)
  # === To run locally. Use load_model instead of load. ===
  # pred.load_model(model_path="/model_path/")  # instead of pred.load()
  pred.load()
  pred.run_api(port=5000)
