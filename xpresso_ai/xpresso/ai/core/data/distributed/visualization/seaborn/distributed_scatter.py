"""
 Scatter plot for distributed dataset
"""
__all__ = ['distributed_scatter']
__author__ = 'Sanyog Vyawahare'

import pandas as pd
import numpy as np
import seaborn as sns
from pyspark.sql import functions as F
from pyspark.ml.feature import Bucketizer
from pyspark.ml.pipeline import Pipeline

from xpresso.ai.core.commons.exceptions.xpr_exceptions import InputLengthMismatch
from xpresso.ai.core.data.visualization import utils
from xpresso.ai.core.data.visualization.seaborn.res.plot_factory import Plot


class NewPlot(Plot):
    """
    Generates a scatter plot
    Attributes:
        plot(:obj: matplotlib.axes): Matplotlib axes which stores the plot
    Args:
        input_data(Dataframe): Input data
        plot_title(str): Title to the plot
        output_format (str): html or png plots to be generated
        file_name(str): File name of the plot to be stored
        axes_labels(dict): Dictionary of x_label, y_label and
            target_label
        output_path(str): path where the html/png plots to be stored
        num_of_bins : Number of bins
    """

    def __init__(self, input_data, plot_title=None,
                 output_format=utils.HTML,
                 file_name=None, axes_labels=None,
                 output_path=utils.DEFAULT_IMAGE_PATH, auto_render=False, num_of_bins=30):
        super().__init__()
        sns.set()
        if axes_labels:
            self.axes_labels = axes_labels
        if plot_title:
            self.plot_title = plot_title
        col1 = input_data.columns[0]
        col2 = input_data.columns[1]
        if len(input_data[col1]) != len(input_data[col2]):
            raise InputLengthMismatch()
        # Process input data
        self.plot = self.scatterplot(input_data._sdf, col1, col2, num_of_bins)
        self.plot.set_ylim(input_data[col2].min(), input_data[col2].max())
        self.plot.set_xlim(input_data[col1].min(), input_data[col1].max())
        if auto_render:
            self.render(output_format=output_format, output_path=output_path,
                        file_name=file_name)

    def strat_scatterplot(self, sdf, col1, col2, num_of_bins=30):
        """
        Pipeline for bucketing the data into bins

        :param sdf: Internal spark dataframe
        :param col1: Column no 1
        :param col2: Column no 2
        :param num_of_bins: Number of bins
        :return: Model pipline and total count of values in sdf
        """
        stages = []
        for col in [col1, col2]:
            splits = np.linspace(*sdf.agg(F.min(col),
                                          F.max(col)).rdd.map(tuple).collect()[0], num_of_bins + 1)
            bucket_name = '__{}_bucket'.format(col)
            stages.append(Bucketizer(splits=splits,
                                     inputCol=col,
                                     outputCol=bucket_name,
                                     handleInvalid="skip"))

        pipeline = Pipeline(stages=stages)
        model = pipeline.fit(sdf)
        return model, sdf.count()

    def scatterplot(self, sdf, col1, col2, num_of_bins):
        """
        Plots Scatter plot graph for the data

        :param sdf: internal Spark dataframe from koalas
        :param col1: column no 1
        :param col2: column no 2
        :param num_of_bins: Number of bins
        :return: Scatter plot
        """

        model, total = self.strat_scatterplot(sdf, col1, col2, num_of_bins=num_of_bins)

        bucket_name1, bucket_name2 = '__{}_bucket'.format(col1), '__{}_bucket'.format(col2)
        splits = [bucket.getSplits() for bucket in model.stages]
        splits = [list(map(np.mean, zip(split[1:], split[:-1]))) for split in splits]
        splits1 = pd.DataFrame({bucket_name1: np.arange(0, num_of_bins), col1: splits[0]})
        splits2 = pd.DataFrame({bucket_name2: np.arange(0, num_of_bins), col2: splits[1]})

        colnames = [bucket_name1, bucket_name2]
        result = model.transform(sdf).select(colnames).groupby(colnames).agg(
            F.count('*').alias('count')).toPandas().sort_values(by=colnames)

        df_counts = result.merge(splits1).merge(splits2)[[col1, col2, 'count']].rename(
            columns={'count': 'Proportion'})
        df_counts.loc[:, 'Proportion'] = df_counts.Proportion.apply(
            lambda p: round(p / total, 4))
        data = df_counts
        return sns.scatterplot(data=data, x=col1, y=col2, size='Proportion',
                               legend='brief', hue='Proportion')
