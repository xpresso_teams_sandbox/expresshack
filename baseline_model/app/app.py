"""
This is the implementation of data preparation for sklearn
"""

import sys
import logging
import time
import json
import numpy as np
import pickle
import os

from sklearn import metrics
import datetime as dt
from datetime import timedelta
from sklearn.metrics import mean_squared_log_error
from statsmodels.tsa.api import Holt
import pandas as pd
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "### Author ###"

logger = XprLogger("baseline_model", level=logging.INFO)


class BaselineModel(AbstractPipelineComponent):
  """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """
  def __init__(self, config_file):
    super().__init__(name="BaselineModel")
    """ Initialize all the required constansts and data her """
    self.preprocess_data = pd.read_csv(config_file['pre_processed_path'])
    # Holts Winter params
    self.holts_smoothing_level = config_file['holts_winter']["smoothing_level"]
    self.holts_smoothing_slope = config_file['holts_winter']["smoothing_slope"]
    self.model_file_dict = {}

  def spilt_train_valid(self, data, p):
    """Method used to split dataset into train and test files

          Args:
              data (csv): Input csv file to split into train and test
              p (float): train test split ratio
          """
    data = data.sort_values(by=['Date'])
    train = data[:int(data.shape[0] * p)]
    valid = data[int(data.shape[0] * p):]
    return train, valid

  def train_holt(self, p=0.9):
      """Method used to train Holt

      Args:
          p (float, optional): train test split ratio. Defaults to 0.9.
      """
    # splitting data into train and test:
    train_data, valid_data = self.spilt_train_valid(self.preprocess_data, p)

    rmse_list = []

    for i, c in enumerate(self.preprocess_data.Country.unique()):
      try:
        train_series = train_data.loc[train_data.Country ==
                                      c].incremental_case_count
        valid_series = valid_data.loc[valid_data.Country ==
                                      c].incremental_case_count
        model = Holt(train_series.values).fit(
          smoothing_level=self.holts_smoothing_level,
          smoothing_slope=self.holts_smoothing_slope)
        predict_count = valid_series.shape[0]

        # forecast values
        forecast_result = model.forecast(predict_count)

        # if forecast is negative make it zero
        npos = np.where(forecast_result < 0)[0]
        for i in npos:
          forecast_result[i] = 0
        rmsle = np.sqrt(
          mean_squared_log_error(valid_series.values, forecast_result))
        rmse_list.append(rmsle)
        self.model_file_dict[c] = model
      except Exception as e:
        logging.error(e)

    return rmse_list, (np.sqrt(np.mean(rmse_list)))

  def start(self, run_name):
    """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
    super().start(xpresso_run_name=run_name)
    # === Your start code base goes here ===
    start_time = time.time()
    rmse_list, overall_error = self.train_holt()

    end_time = time.time()
    logging.info("root mean square error: ", overall_error)

    report_status = {
      "status": {
        "status": "Training completed"
      },
      "metric": {
        "duration": "{:.24f} min".format((end_time - start_time) / 60),
        "overall_error": "{:.10f}".format(overall_error)
      }
    }
    self.send_metrics(report_status)
    logging.info("Training has been completed ....")
    logging.info("Total time taken for training: {:.4f} min".format(
      (end_time - start_time) / 60))
    logging.info("Overall Error: {:.10f}".format(overall_error))

    self.completed()

  def send_metrics(self, report_status):
    """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
    # report_status = {
    #     "status": {"status": "data_preparation"},
    #     "metric": {"metric_key": 1}
    # }
    self.report_status(status=report_status)

  def completed(self, push_exp=True):
    """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned

        """
    # === Your start code base goes here ===
    # self.OUTPUT_DIR = 'D:/Hackathon/'
    if not os.path.exists(self.OUTPUT_DIR):
      os.makedirs(self.OUTPUT_DIR)

    # Save files
    model_path = os.path.join(self.OUTPUT_DIR, "model.pickle")
    with open(model_path, 'wb') as f:
      pickle.dump(self.model_file_dict, f, protocol=4)
    logging.info("Model file saved at {}".format(model_path))
    print("Model file saved at {}".format(model_path))

    file_path = os.path.join(self.OUTPUT_DIR, 'covid19_pre_processed.xlsx')
    self.preprocess_data.to_excel(file_path, index=False)
    logging.info("covid pre-processed file saved at {}".format(file_path))
    print("covid pre-processed file saved at {}".format(file_path))
    super().completed(push_exp=push_exp)

  def terminate(self):
    """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
    # === Your start code base goes here ===
    super().terminate()

  def pause(self, push_exp=True):
    """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
    # === Your start code base goes here ===
    super().pause()

  def restart(self):
    """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
    # === Your start code base goes here ===
    super().restart()


if __name__ == "__main__":
  # To run locally. Use following command:
  # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/app.py
  config = {}
  with open("config/dev.json") as conf_file:
    config = json.load(conf_file)
  logging.info("Config loaded")

  data_prep = BaselineModel(config)
  if len(sys.argv) >= 2:
    data_prep.start(run_name=sys.argv[1])
  else:
    data_prep.start(run_name="")
