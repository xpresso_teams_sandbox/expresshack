"""
This is the implementation of data preparation for sklearn
"""

import sys
import logging
import json
import pandas as pd
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger


__author__ = "### Author ###"

logger = XprLogger("data_prepare",level=logging.INFO)


class DataPrepare(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self, config_file):
        super().__init__(name="DataPrepare")
        """ Initialize all the required constansts and data her """
        self.training_data_path = config['train_file_path']
        self.pre_processed_data = config['pre_processed_path']


    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        super().start(xpresso_run_name=run_name)
        # === Your start code base goes here ===
        covid_full_data = pd.read_excel(self.training_data_path)
        covid_full_data["Date"] = pd.to_datetime(covid_full_data["Date"])

        # Filling null values for Province column
        covid_full_data.Province.fillna(".", inplace=True)

        # removing unneccesary data
        covid_full_data = covid_full_data.loc[
            ~covid_full_data.Country.isin(['Diamond Princess', 'MS Zaandam'])]

        # grouping data by country, removing provinces
        covid_full_data = covid_full_data.groupby(['Country',
                                                   'Date']).sum().reset_index()

        # finding incremental counts of each day from the cummulative count in "Confirmed" column
        group_c = covid_full_data.groupby(['Country'])
        covid_full_data['incremental_case_count'] = group_c.Confirmed.diff()
        covid_full_data.incremental_case_count.fillna(0, inplace=True)
        covid_full_data.to_csv(self.pre_processed_data, index=False)

        self.completed()

    def send_metrics(self):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        report_status = {
            "status": {"status": "data_preparation"},
            "metric": {"metric_key": 1}
        }
        self.report_status(status=report_status)

    def completed(self, push_exp=False):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned

        """
        # === Your start code base goes here ===
        super().completed(push_exp=push_exp)

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        super().terminate()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        super().pause()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        super().restart()


if __name__ == "__main__":
    # To run locally. Use following command:/
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/app.py
    config = {}
    with open("config/dev.json") as conf_file:
        config = json.load(conf_file)
    logging.info("Config loaded")

    data_prep = DataPrepare(config)
    #handle dynamic args for train file...2nd will be path...1st for xpresso
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
